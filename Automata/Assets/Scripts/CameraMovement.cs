﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour 
{
	#region STATIC VARIABLES
	
	public static CameraMovement Current;
	
	#endregion

	#region PUBLIC VARIABLES

	public float Speed = 2.0f;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion

	#region PRIVATE VARIABLES

	private bool click = false;
	private Vector3 target = Vector3.zero;

	#endregion

	#region MONOBEHAVIOUR

	/// <summary>
	/// Awakes this instance.
	/// </summary>
	private void Awake()
	{
		Current = this;
	}

	/// <summary>
	/// Updates this instance, called once per frame.
	/// </summary>
	private void Update()
	{
		if(Input.GetMouseButtonDown(0))
			click = true;

		if(Input.GetMouseButtonUp(0))
			click = false;

		if(click)
		{
			transform.RotateAround(target, Vector3.up, Input.GetAxis("Mouse X") * Speed);
		}
	}

	#endregion
	
	#region PUBLIC METHODS

	/// <summary>
	/// Sets the target point to rotate around.
	/// </summary>
	/// <param name="_Position">The target position.</param>
	public void SetTarget(Vector3 _Position)
	{
		Camera.main.transform.rotation = Quaternion.identity;
		target = _Position;
	}

	#endregion
	
	#region PRIVATE METHODS
	#endregion
}
