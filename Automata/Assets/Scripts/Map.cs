﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class handling the creation of a 3d map.
/// </summary>
public class Map : MonoBehaviour 
{
	#region STATIC VARIABLES

	public static Map Current;

	#endregion

	#region PUBLIC VARIABLES

	public int ChunkSize = 30;

	public Chunk ChunkPrefab;

	#endregion

	#region MONOBEHAVIOUR

	/// <summary>
	/// Awakes this instance.
	/// </summary>
	void Awake()
	{
		if(Current != null)
		{
			Debug.LogWarning(this.name + ": Overwriting existing map '" + Current.name + "' with this one.");
			Destroy(Current);
		}

		Current = this;
	}

	#endregion
	
	#region PUBLIC METHODS

	/// <summary>
	/// Generates a map from the specified grid.
	/// </summary>
	/// <param name="_Grid">The grid data to create a map from.</param>
	/// <param name="_Inverse">Specifies whether the map is inverted.</param>
	/// <param name="_Parent">The parent to be used for all chunks.</param>
	public void Generate(bool[,,] _Grid, bool _Inverse, Transform _Parent)
	{
		int count = 1;
		int total = 1;

		// Get largest dimension
		int size = Mathf.Max(_Grid.GetLength(0), _Grid.GetLength(1), _Grid.GetLength(2));

		//Debug.Log(this.name + ": Largest dimension: " + size);

		// Get necessary amount of chunks
		if(size > ChunkSize)
		{
			count = Mathf.CeilToInt((float)size / (float)ChunkSize);
			total = (int)Mathf.Pow((float)count, 3);

			size = ChunkSize;
		}

		//Debug.Log(this.name + ": Size: " + size + ", Count: " + count + ", Total: " + total);

		// Split grid into chunk sized grids
		int xIndex = 0;
		int yIndex = 0;
		int zIndex = 0;
		Chunk chunk;
		byte[,,] grid = new byte[size, size, size];
		// Iterate through all chunks
		for(int i = 0; i < total; i++)
		{
			// Iterate through all three dimensions and copy the grid's value to the current chunk's grid
			for(int x = xIndex * size; x < (xIndex + 1) * size; x++)
			{
				for(int y = yIndex * size; y < (yIndex + 1) * size; y++)
				{
					for(int z = zIndex * size; z < (zIndex + 1) * size; z++)
					{
						// Set all cells outside the actual grid to the maximum byte value.
						// This ensures that inversion of the map will not result in a giant block of stone outside the actual grid.
						if(x >= _Grid.GetLength(0) || y >= _Grid.GetLength(1) || z >= _Grid.GetLength(2))
							grid[x % size, y % size, z % size] = byte.MaxValue;
						// Convert the actual grid data from bool to byte
						else
							grid[x % size, y % size, z % size] = (byte) (_Grid[x, y, z] ? 1 : 0);
					}
				}
			}

			// Create new chunk
			chunk = Instantiate(ChunkPrefab, new Vector3(xIndex * size, yIndex * size, zIndex * size), Quaternion.identity) as Chunk;
			if(chunk != null)
			{
				//Debug.Log(this.name + ": Creating chunk at (" + xIndex + ", " + yIndex + ", " + zIndex + ").");

				chunk.name = "Chunk " + i;
				if(_Parent != null)
					chunk.transform.parent = _Parent;
				else
					chunk.transform.parent = transform;

				// Initialize the current chunk with the created grid part
				chunk.Initialize(grid, _Inverse);
			}
			else
				Debug.LogError(this.name + ": Unable to create chunk at (" + xIndex + ", " + yIndex + ", " + zIndex + ").");

			// Increase indizes
			zIndex++;
			if(zIndex >= count)
			{
				zIndex = 0;
				yIndex++;
				if(yIndex >= count)
				{
					yIndex = 0;
					xIndex++;
				}
			}
		}
	}

	#endregion
}
