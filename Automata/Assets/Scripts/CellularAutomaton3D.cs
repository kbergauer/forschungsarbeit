using UnityEngine;
using System;
using System.Collections;
using System.Text.RegularExpressions;
using Random = UnityEngine.Random;

/// <summary>
/// Class generating a 3d cave using a cellular automaton.
/// </summary>
public class CellularAutomaton3D : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	// Debug flags
	public bool ShowDebugOutput = false;
	public bool ShowOnlyResult = true;

	// Automaton parameters
	public float InitialBirthChance = 0.45f;
	public Vector2 BirthLimit = new Vector2(14.0f, 26.0f);
	public Vector2 DeathLimit = new Vector2(7.0f, 27.0f);
	public int SimulationSteps = 15;
	public int GridSize = 20;

	// Visual parameters
	public Material RockMaterial;
	public Color NormalColor = Color.white;
	public Color InvertedColor = Color.yellow;

	#endregion

	#region PROTECTED AND PRIVATE VARIABLES

	private bool inverted = true;
	private int generating = 0;
	private bool pauseGeneration = false;
	
	private int currentStep = 0;

	private bool[,,] grid;
	private bool[,,] newGrid;

	private Transform tempTransform;
	private GameObject tempGameObject;
	
	private Vector3 cellPosition = Vector3.zero;

	private float generationTime = 0.0f;
	private DateTime startTime;

	private string[] inputs = new string[10];

	#endregion



	#region MONOBEHAVIOUR

	/// <summary>
	/// Starts this instance.
	/// </summary>
	private void Start()
	{
		// Abort generation if no map is assigned
		if(Map.Current == null)
		{
			Debug.LogError(this.name + ": No map available, stopping generation.");
			gameObject.SetActive(false);
		}

		// Setup gui input
		inputs[0] = InitialBirthChance.ToString();
		inputs[1] = ((int)BirthLimit.x).ToString();
		inputs[2] = ((int)BirthLimit.y).ToString();
		inputs[3] = ((int)DeathLimit.x).ToString();
		inputs[4] = ((int)DeathLimit.y).ToString();
		inputs[5] = SimulationSteps.ToString();
		inputs[6] = GridSize.ToString();
		inputs[7] = cellPosition.x.ToString();
		inputs[8] = cellPosition.y.ToString();
		inputs[9] = cellPosition.z.ToString();

		// Position main camera
		Camera.main.transform.position = new Vector3(GridSize / 2.0f, GridSize / 2.0f, -GridSize);
		CameraMovement.Current.SetTarget(new Vector3(GridSize / 2.0f, GridSize / 2.0f, GridSize / 2.0f));

		// Setup grid
		grid = new bool[GridSize, GridSize, GridSize];
	}

	/// <summary>
	/// Displays the GUI.
	/// </summary>
	private void OnGUI()
	{
		if(pauseGeneration)
		{
			GUI.Box(new Rect((Screen.width - 200) / 2, (Screen.height - 200) / 2, 200, 170), "Warning");
			GUILayout.BeginArea(new Rect(((Screen.width - 200) / 2) + 5, ((Screen.height - 200) / 2) + 5, 190, 160));
			GUILayout.Space(25);
			GUILayout.Label("You have disabled ShowOnlyResult with a GridSize above 60. Please be aware that this requires huge amounts of memory and can cause Unity to crash. Proceed with caution!");

			GUILayout.Space(10);

			GUILayout.BeginHorizontal();
			if(GUILayout.Button("Continue", GUILayout.Width(90)))
				pauseGeneration = false;
			if(GUILayout.Button("Abort", GUILayout.Width(90)))
				Reset();
			GUILayout.EndHorizontal();

			GUILayout.EndArea();
		}

		GUI.Box(new Rect(0, 0, 200, 380), "");
		GUILayout.BeginArea(new Rect(5, 5, 190, 370));

		if(generating > 0)
		{
			if(generating >= 1)
				GUILayout.Label("Generating..");

			if(generating >= 2)
				GUILayout.Label("Instantiating..");
		}
		else
		{
			if(GUILayout.Button("Generate"))
			{
				Reset();
				StartCoroutine(Generate());
				generating++;
				startTime = DateTime.Now;
			}
			GUILayout.BeginHorizontal();
			GUILayout.Label("Initial birth chance", GUILayout.Width(150));
			inputs[0] = GUILayout.TextField(inputs[0]);
			inputs[0] = Regex.Replace(inputs[0], @"[^0-9 .]", "");
			if(!string.IsNullOrEmpty(inputs[0]))
				float.TryParse(inputs[0], out InitialBirthChance);
			GUILayout.EndHorizontal();
			GUILayout.Label("Birth limit");
			GUILayout.BeginHorizontal();
			GUILayout.Space(25);
			inputs[1] = GUILayout.TextField(inputs[1], GUILayout.Width(50));
			inputs[1] = Regex.Replace(inputs[1], @"[^0-9 ]", "");
			if(!string.IsNullOrEmpty(inputs[1]))
				float.TryParse(inputs[1], out BirthLimit.x);
			GUILayout.Label("to", GUILayout.Width(20));
			inputs[2] = GUILayout.TextField(inputs[2], GUILayout.Width(50));
			inputs[2] = Regex.Replace(inputs[2], @"[^0-9 ]", "");
			if(!string.IsNullOrEmpty(inputs[2]))
				float.TryParse(inputs[2], out BirthLimit.y);
			GUILayout.EndHorizontal();
			GUILayout.Label("Death limit");
			GUILayout.BeginHorizontal();
			GUILayout.Space(25);
			GUILayout.Label("-Infinity to", GUILayout.Width(60));
			inputs[3] = GUILayout.TextField(inputs[3], GUILayout.Width(50));
			inputs[3] = Regex.Replace(inputs[3], @"[^0-9 ]", "");
			if(!string.IsNullOrEmpty(inputs[3]))
				float.TryParse(inputs[3], out DeathLimit.x);
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			GUILayout.Space(25);
			inputs[4] = GUILayout.TextField(inputs[4], GUILayout.Width(50));
			inputs[4] = Regex.Replace(inputs[4], @"[^0-9 ]", "");
			if(!string.IsNullOrEmpty(inputs[4]))
				float.TryParse(inputs[4], out DeathLimit.y);
			GUILayout.Label("to Infinity", GUILayout.Width(75));
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			GUILayout.Label("Simulation steps", GUILayout.Width(125));
			inputs[5] = GUILayout.TextField(inputs[5], GUILayout.Width(50));
			inputs[5] = Regex.Replace(inputs[5], @"[^0-9 ]", "");
			if(!string.IsNullOrEmpty(inputs[5]))
				int.TryParse(inputs[5], out SimulationSteps);
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			GUILayout.Label("Grid size", GUILayout.Width(125));
			inputs[6] = GUILayout.TextField(inputs[6], GUILayout.Width(50));
			inputs[6] = Regex.Replace(inputs[6], @"[^0-9 ]", "");
			if(!string.IsNullOrEmpty(inputs[6]))
				int.TryParse(inputs[6], out GridSize);
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			GUILayout.Label("Inverted", GUILayout.Width(125));
			inverted = GUILayout.Toggle(inverted, "");
			GUILayout.EndHorizontal();

			GUILayout.Space(20);

			if(GUILayout.Button("Initialize"))
				Initialize(true);
			if(GUILayout.Button("Next step"))
			{
				currentStep++;
				SimulateStep(true);
			}
			if(GUILayout.Button("Clean up"))
				CleanUp(true);

			GUILayout.Label("Generation time: " + generationTime.ToString("0.00") + " secs");
		}

		GUILayout.EndArea();

		if(GUI.Button(new Rect(5, Screen.height - 5 - 20, 100, 20), "Reset"))
			Reset();

		if(GUI.Button(new Rect(110, Screen.height - 5 - 20, 100, 20), "Invert"))
		{
			inverted = !inverted;

			InstantiateGrid(grid, "Current ");
		}

		if(GUI.Button(new Rect(5, Screen.height - 10 - 40, 100, 20), "Neighbors"))
		{
			Debug.Log(this.name + ": Neighbors of cell " + cellPosition + ": " + CountLivingNeighbors(grid, (int)cellPosition.x, (int)cellPosition.y, (int)cellPosition.z));
		}
		inputs[7] = GUI.TextField(new Rect(110, Screen.height - 10 - 40, 50, 20), inputs[7]);
		inputs[7] = Regex.Replace(inputs[7], @"[^0-9 ]", "");
		if(!string.IsNullOrEmpty(inputs[7]))
			float.TryParse(inputs[7], out cellPosition.x);
		inputs[8] = GUI.TextField(new Rect(165, Screen.height - 10 - 40, 50, 20), inputs[8]);
		inputs[8] = Regex.Replace(inputs[8], @"[^0-9 ]", "");
		if(!string.IsNullOrEmpty(inputs[8]))
			float.TryParse(inputs[8], out cellPosition.y);
		inputs[9] = GUI.TextField(new Rect(220, Screen.height - 10 - 40, 50, 20), inputs[9]);
		inputs[9] = Regex.Replace(inputs[9], @"[^0-9 ]", "");
		if(!string.IsNullOrEmpty(inputs[9]))
			float.TryParse(inputs[9], out cellPosition.z);
	}

	#endregion



	#region PROTECTED AND PRIVATE METHODS

	/// <summary>
	/// Generates a cave with the current parameters.
	/// </summary>
	private IEnumerator Generate()
	{
		// Safety measures
		if(!ShowOnlyResult)
		{
			if(GridSize > 60)
			{
				Debug.LogWarning(this.name + ": You have ShowOnlyResult disabled with a GridSize higher than 60. Be aware that this needs huge amounts of memory and can cause the engine to crash! Please consider only showing the result of your generation.");
				pauseGeneration = true;
				while(pauseGeneration)
				{
					yield return new WaitForEndOfFrame();
				}
			}
			if(Map.Current.ChunkSize > 20)
				Map.Current.ChunkSize = 20;
		}

		// Initialize grid
		Initialize();
		yield return new WaitForEndOfFrame();

		// Update cells SimulationSteps times
		for(currentStep = 0; currentStep < SimulationSteps; currentStep++)
		{
			SimulateStep();
			yield return new WaitForEndOfFrame();
		}

		// Clean result up
		CleanUp();
	}

	/// <summary>
	/// Initialize the grid with random values of true (rock) and false (air).
	/// </summary>
	/// <param name="_Show">If set to <c>true</c> forces the result to be instantiated.</param> 
	private void Initialize(bool _Show = false)
	{
		if(ShowDebugOutput)
			Debug.Log(this.name + ": Initializing grid.");

		// Setup grid
		grid = new bool[GridSize, GridSize, GridSize];

		// Set values to true with InitialBirthChance, otherwise false
		for(int z = 0; z < grid.GetLength(2); z ++)
		{
			for(int y = 0; y < grid.GetLength(1); y++)
			{
				for(int x = 0; x < grid.GetLength(0); x++)
				{
					if(Random.Range(0.0f, 1.0f) <= InitialBirthChance)
						grid[x, y, z] = true;
					else
						grid[x, y, z] = false;
				}
			}
		}

		// Instantiate grid
		if(!ShowOnlyResult || _Show)
			InstantiateGrid(grid, "Init");
	}

	/// <summary>
	/// Executes one simulation step of the automata, updating all cells and generating a new grid.
	/// </summary>
	/// <param name="_Show">If set to <c>true</c> forces the result to be instantiated.</param> 
	private void SimulateStep(bool _Show = false)
	{
		if(ShowDebugOutput)
			Debug.Log(this.name + ": Simulating grid step. (" + (currentStep + 1) + ")");

		// Initialize new grid to save all changes
		newGrid = new bool[grid.GetLength(0), grid.GetLength(1), grid.GetLength(2)];
		int neighbors = 0;

		// Iterate through current grid
		for(int z = 0; z < grid.GetLength(2); z++)
		{
			for(int y = 0; y < grid.GetLength(1); y++)
			{
				for(int x = 0; x < grid.GetLength(0); x++)
				{

					// Tranfer old value to new grid, in case there is no change being made to the cell
					newGrid[x, y, z] = grid[x, y, z];

					// Get living neighbors
					neighbors = CountLivingNeighbors(grid, x, y, z);

					//Debug.Log(this.name + ": (" + x + ", " + y + ", " + z + ") Neighbors: " + neighbors);

					// If cell is alive, check death limits
					if(grid[x, y, z])
					{
						//Debug.Log(this.name + ": (" + x + ", " + y + ", " + z + ") Alive, checking for death limits.");

						// Check for isolation
						if(neighbors <= DeathLimit.x)
						{
							//Debug.Log(this.name + ": (" + x + ", " + y + ", " + z + ") Died of isolation.");
							newGrid[x, y, z] = false;
						}
						// Check for overpopulation
						else if(neighbors >= DeathLimit.y)
						{
							//Debug.Log(this.name + ": (" + x + ", " + y + ", " + z + ") Died of overpopulation.");
							newGrid[x, y, z] = false;
						}
					}
					// If cell is dead, check birth limit
					else if(neighbors >= BirthLimit.x && neighbors <= BirthLimit.y)
					{
						//Debug.Log(this.name + ": (" + x + ", " + y + ", " + z + ") Dead, being born.");
						newGrid[x, y, z] = true;
					}
				}
			}
		}

		// Replace old with new grid
		grid = newGrid;

		// Instantiate new grid hiding the old
		if(!ShowOnlyResult || _Show)
			InstantiateGrid(grid, "Step " + currentStep);
	}

	/// <summary>
	/// Counts the living neighbors of the specified cell.
	/// </summary>
	/// <returns>Returns the number of living cells in a sphere around the cell at position (x, y, z)</returns>
	/// <param name="_Grid">The grid to use</para>.</param>
	/// <param name="_X">The cell's x position.</param>
	/// <param name="_Y">The cell's y position.</param>
	/// <param name="_Z">The cell's z position.</param>
	private int CountLivingNeighbors(bool[,,] _Grid, int _X, int _Y, int _Z)
	{
		int count = 0;
		int neighborX;
		int neighborY;
		int neighborZ;

		// Iterate through all neighbors
		for(int z = -1; z < 2; z++)
		{
			for(int y = -1; y < 2; y++)
			{
				for(int x = -1; x < 2; x++)
				{
					// Ignore center cell
					if(x == 0 && y == 0 && z == 0)
						continue;

					// Get current neighbor's position
					neighborX = _X + x;
					neighborY = _Y + y;
					neighborZ = _Z + z;

					// Count cells off the grid as alive, as well as the outermost border
					if(neighborX < 1 || neighborX >= _Grid.GetLength(0) - 1
						|| neighborY < 1 || neighborY >= _Grid.GetLength(1) - 1
						|| neighborZ < 1 || neighborZ >= _Grid.GetLength(2) - 1)
					{
						count++;
					}
					// Check whether the current cell is alive and add it to the total
					else if(_Grid[neighborX, neighborY, neighborZ])
					{
						count++;
					}
				}
			}
		}

		return count;
	}

	/// <summary>
	/// Cleans the grid up.
	/// </summary>
	/// <param name="_Show">If set to <c>true</c> forces the result to be instantiated.</param> 
	private void CleanUp(bool _Show = false)
	{
		if(ShowDebugOutput)
			Debug.Log(this.name + ": Cleaning up grid.");

		// Solidify borders
		for(int z = 0; z < grid.GetLength(2); z++)
		{
			for(int x = 0; x < grid.GetLength(0); x++)
			{
				grid[x, 0, z] = true;
				grid[x, grid.GetLength(1) - 1, z] = true;
			}
			for(int y = 0; y < grid.GetLength(1); y++)
			{
				grid[0, y, z] = true;
				grid[grid.GetLength(0) - 1, y, z] = true;
			}
		}
		for(int x = 0; x < grid.GetLength(0); x++)
		{
			for(int y = 0; y < grid.GetLength(1); y++)
			{
				grid[x, y, 0] = true;
				grid[x, y, grid.GetLength(2) - 1] = true;
			}
		}

		// End generation and calculate total time taken
		generating++;
		generationTime = (float)DateTime.Now.Subtract(startTime).TotalMilliseconds / 1000.0f;

		if(ShowDebugOutput)
			Debug.Log(this.name + ": Generation done, starting instantiation.");

		// Instantiate final grid hiding the old
		if(!ShowOnlyResult || _Show)
			InstantiateGrid(grid, "CleanUp");
		else
			InstantiateGrid(grid, "Result");

		generating = 0;
	}

	/// <summary>
	/// Instantiates the grid, parenting it to an empty game object.
	/// </summary>
	/// <param name="_Grid">The grid to instantiate.</param>
	/// <param name="_Name">The name of the empty parent game object.</param>
	private void InstantiateGrid(bool[,,] _Grid, string _Name)
	{
		// Hide other grids and delete copies
		foreach(Transform child in transform)
		{
			if(child.gameObject.activeInHierarchy)
			{
				if(child.gameObject.name == _Name)
					Destroy(child.gameObject);
				else
					child.gameObject.SetActive(false);
			}
		}

		// Create new parent object
		tempGameObject = new GameObject(_Name);
		tempGameObject.transform.parent = transform;

		// Create actual grid
		if(!inverted)
			InstantiateGrid(_Grid, tempGameObject.transform);
		// Create inverted grid, if specified
		else
		{
			tempGameObject.name += " Inverted";
			InstantiateGridInverse(_Grid, tempGameObject.transform);
		}
	}

	/// <summary>
	/// Instantiates the grid, parenting it to the specified transform.
	/// </summary>
	/// <param name="_Grid">The grid to instantiate.</param>
	/// <param name="_Parent">The parent transform.</param>
	private void InstantiateGrid(bool[,,] _Grid, Transform _Parent)
	{
		if(ShowDebugOutput)
			Debug.Log(this.name + ": Instantiating grid.");

		Map.Current.Generate(_Grid, false, _Parent);

		RockMaterial.color = NormalColor;
	}
	
	/// <summary>
	/// Instantiates the inverted grid, parenting it to the specified transform.
	/// </summary>
	/// <param name="_Grid">The grid to instantiate.</param>
	/// <param name="_Parent">The parent transform.</param>
	private void InstantiateGridInverse(bool[,,] _Grid, Transform _Parent)
	{
		if(ShowDebugOutput)
			Debug.Log(this.name + ": Instantiating inverse grid.");

		// Generate map
		Map.Current.Generate(_Grid, true, _Parent);

		RockMaterial.color = InvertedColor;
	}

	/// <summary>
	/// Resets the script.
	/// </summary>
	private void Reset()
	{
		currentStep = 0;

		// Reset camera
		Camera.main.transform.position = new Vector3(GridSize / 2.0f, GridSize / 2.0f, -GridSize);
		CameraMovement.Current.SetTarget(new Vector3(GridSize / 2.0f, GridSize / 2.0f, GridSize / 2.0f));
		
		StopAllCoroutines();
		foreach(Transform child in transform)
			Destroy(child.gameObject);

		generating = 0;
		pauseGeneration = false;
	}

	#endregion
}
