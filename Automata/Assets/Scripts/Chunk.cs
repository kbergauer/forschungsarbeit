﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class holding a chunk of cubes, handling its rendering.
/// </summary>
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class Chunk : MonoBehaviour 
{
	#region STATIC VARIABLES

	// List of active chunks, necessary due to vertex limitation of single meshes
	public static List<Chunk> chunks = new List<Chunk>();

	public static int Width
	{
		get { return Map.Current.ChunkSize;	}
	}
	public static int Height
	{
		get { return Map.Current.ChunkSize;	}
	}

	#endregion

	#region PUBLIC VARIABLES

	public bool IsInverted = false;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion

	#region PRIVATE VARIABLES

	// Block data
	private byte[,,] grid;

	// Rendering data
	private Mesh visualMesh;
	//private MeshRenderer meshRenderer;
	private MeshFilter meshFilter;

	#endregion

	#region MONOBEHAVIOUR
	#endregion

	#region STATIC METHODS

	/// <summary>
	/// Finds the chunk corresponding to the specified position.
	/// </summary>
	/// <returns>The chunk.</returns>
	/// <param name="_Position">The position.</param>
	public static Chunk FindChunk(Vector3 _Position)
	{
		Vector3 chunkPosition;

		for(int i = 0; i < chunks.Count; i++)
		{
			chunkPosition = chunks[i].transform.position;

			// Check whether the target position is outside the current chunk's bounds
			if( _Position.x < chunkPosition.x || _Position.y < chunkPosition.y || _Position.z < chunkPosition.z
			   || _Position.x > chunkPosition.x + Width || _Position.y > chunkPosition.y + Height || _Position.z > chunkPosition.z + Width )
				continue;

			// If not, return current chunk
			return chunks[i];
		}

		// No chunk at target position
		return null;
	}

	#endregion
	
	#region PUBLIC METHODS

	/// <summary>
	/// Initialize the chunk with the specified grid data.
	/// </summary>
	/// <param name="_Grid">The grid data to use for the chunk.</param>
	/// <param name="_Inverse">Specifies whether the chunk is inverted.</param>
	public void Initialize(byte[,,] _Grid, bool _Inverse)
	{
		// Add chunk to the list of active chunks
		chunks.Add(this);
		
		// Grab rendering components
		meshFilter = GetComponent<MeshFilter>();
		//meshRenderer = GetComponent<MeshRenderer>();

		// Set grid
		grid = _Grid;

		// Set inverse
		IsInverted = _Inverse;

		// Create mesh from grid data
		StartCoroutine(CreateVisualMesh());
	}

	/// <summary>
	/// Creates a new visual mesh from the current grid data.
	/// </summary>
	public IEnumerator CreateVisualMesh()
	{
		// Create new mesh instance
		visualMesh = new Mesh();

		// Necessary helper lists
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uvs = new List<Vector2>();
		List<int> triangles = new List<int>();

		// 
		for(int x = 0; x < Width; x++)
		{
			for(int y = 0; y < Height; y++)
			{
				for(int z = 0; z < Width; z++)
				{
					// Skip air blocks
					if(IsTransparent(x, y, z))
						continue;

					// Build left face
					if(IsTransparent(x - 1, y, z))
						BuildFace(new Vector3(x, y, z), Vector3.up, Vector3.forward, false, vertices, uvs, triangles);
					// Build right face
					if(IsTransparent(x + 1, y, z))
						BuildFace(new Vector3(x + 1, y, z), Vector3.up, Vector3.forward, true, vertices, uvs, triangles);

					// Build bottom face
					if(IsTransparent(x, y - 1, z))
						BuildFace(new Vector3(x, y, z), Vector3.forward, Vector3.right, false, vertices, uvs, triangles);
					// Build top face
					if(IsTransparent(x, y + 1, z))
						BuildFace(new Vector3(x, y + 1, z), Vector3.forward, Vector3.right, true, vertices, uvs, triangles);

					// Build back face
					if(IsTransparent(x, y, z - 1))
						BuildFace(new Vector3(x, y, z), Vector3.up, Vector3.right, true, vertices, uvs, triangles);
					// Build front face
					if(IsTransparent(x, y, z + 1))
						BuildFace(new Vector3(x, y, z + 1), Vector3.up, Vector3.right, false, vertices, uvs, triangles);
				}
			}
		}

		// Set visual mesh parameters to newly calculated values
		visualMesh.vertices = vertices.ToArray();
		visualMesh.uv = uvs.ToArray();
		visualMesh.triangles = triangles.ToArray();
		visualMesh.RecalculateBounds();
		visualMesh.RecalculateNormals();
		visualMesh.name = "Chunk";

		// Set mesh filter to use the new mesh
		meshFilter.mesh = visualMesh;

		yield return 0;
	}

	#endregion
	
	#region PRIVATE METHODS

	/// <summary>
	/// Builds a mesh face.
	/// </summary>
	/// <param name="_Corner">The corner.</param>
	/// <param name="_Up">The up direction.</param>
	/// <param name="_Right">The right direction.</param>
	/// <param name="_Reversed">If set to <c>true</c>, the face is reversed.</param>
	/// <param name="_Vertices">The vertices.</param>
	/// <param name="_UVs">The UVs.</param>
	/// <param name="_Triangles">The triangles.</param>
	private void BuildFace(Vector3 _Corner, Vector3 _Up, Vector3 _Right, bool _Reversed, List<Vector3> _Vertices, List<Vector2> _UVs, List<int> _Triangles)
	{
		int index = _Vertices.Count;

		// Add all vertices
		_Vertices.Add(_Corner);
		_Vertices.Add(_Corner + _Up);
		_Vertices.Add(_Corner + _Up + _Right);
		_Vertices.Add(_Corner + _Right);

		// Add all uv coordinates for texturing
		_UVs.Add(new Vector2(0.0f, 0.0f));
		_UVs.Add(new Vector2(0.0f, 1.0f));
		_UVs.Add(new Vector2(1.0f, 1.0f));
		_UVs.Add(new Vector2(1.0f, 0.0f));

		// Create the two triangles composing a face
		if(_Reversed)
		{
			_Triangles.Add(index + 0);
			_Triangles.Add(index + 1);
			_Triangles.Add(index + 2);
			_Triangles.Add(index + 2);
			_Triangles.Add(index + 3);
			_Triangles.Add(index + 0);
		}
		else
		{
			_Triangles.Add(index + 1);
			_Triangles.Add(index + 0);
			_Triangles.Add(index + 2);
			_Triangles.Add(index + 3);
			_Triangles.Add(index + 2);
			_Triangles.Add(index + 0);
		}
	}

	/// <summary>
	/// Determines whether the grid value at the specified coordinates is transparent.
	/// </summary>
	/// <returns><c>true</c> if the grid value at the specified coordinates is transparent; otherwise, <c>false</c>.</returns>
	/// <param name="_X">The x coordinate.</param>
	/// <param name="_Y">The y coordinate.</param>
	/// <param name="_Z">The z coordinate.</param>
	private bool IsTransparent(int _X, int _Y, int _Z)
	{
		byte block = GetByte(_X, _Y, _Z);

		// Out of bounds blocks are always transparent
		if(block == byte.MaxValue)
			return true;

		bool result = true;
		if(block > 0)
			result = false;
		return IsInverted ? !result : result;
	}

	/// <summary>
	/// Gets the byte of the specified grid coordinate.
	/// </summary>
	/// <returns>The byte.</returns>
	/// <param name="_X">The x coordinate.</param>
	/// <param name="_Y">The y coordinate.</param>
	/// <param name="_Z">The z coordinate.</param>
	private byte GetByte(int _X, int _Y, int _Z)
	{
		if(_X < 0 || _Y < 0 || _Z < 0
		   || _X >= grid.GetLength(0) || _Y >= grid.GetLength(1) || _Z >= grid.GetLength(2))
			return byte.MaxValue;

		return grid[_X, _Y, _Z];
	}

	#endregion
}
