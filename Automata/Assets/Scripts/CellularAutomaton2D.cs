﻿using UnityEngine;
using System;
using System.Collections;
using System.Text.RegularExpressions;
using Random = UnityEngine.Random;

/// <summary>
/// Class generating a 2d cave using a cellular automaton.
/// </summary>
public class CellularAutomaton2D : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	public bool ShowDebugOutput = false;
	public bool ShowOnlyResult = true;

	public float InitialBirthChance = 0.45f;
	public Vector2 BirthLimit = new Vector2(6.0f, 8.0f);
	public Vector2 DeathLimit = new Vector2(2.0f, 9.0f);
	public int SimulationSteps = 15;
	public int GridSize = 20;

	public Transform RockPrefab;

	#endregion

	#region PROTECTED AND PRIVATE VARIABLES

	private bool generating = false;
	private int currentStep = 0;

	private bool[,] grid;
	private bool[,] newGrid;

	private Transform tempTransform;
	private GameObject tempGameObject;

	private float generationTime = 0.0f;
	private DateTime startTime;

	private string[] inputs = new string[7];

	#endregion



	#region MONOBEHAVIOUR

	/// <summary>
	/// Starts this instance.
	/// </summary>
	private void Start()
	{
		// Abort generation of no prefab is assigned
		if(RockPrefab == null)
		{
			Debug.LogError(this.name + ": No rock prefab assigned, stopping generation.");
			gameObject.SetActive(false);
		}

		// Setup gui input
		inputs[0] = InitialBirthChance.ToString();
		inputs[1] = ((int)BirthLimit.x).ToString();
		inputs[2] = ((int)BirthLimit.y).ToString();
		inputs[3] = ((int)DeathLimit.x).ToString();
		inputs[4] = ((int)DeathLimit.y).ToString();
		inputs[5] = SimulationSteps.ToString();
		inputs[6] = GridSize.ToString();

		// Position main camera
		Camera.main.transform.position = new Vector3(GridSize / 2.0f, GridSize / 2.0f, -GridSize);
	}

	/// <summary>
	/// Displays the GUI.
	/// </summary>
	private void OnGUI()
	{
		GUI.Box(new Rect(0, 0, 200, 350), "");
		GUILayout.BeginArea(new Rect(5, 5, 190, 340));

		if(generating)
			GUILayout.Label("Generating..");
		else
		{
			if(GUILayout.Button("Generate"))
			{
				Reset();
				generating = true;
				startTime = DateTime.Now;
				StartCoroutine(Generate());
			}
			GUILayout.BeginHorizontal();
			GUILayout.Label("Initial birth chance", GUILayout.Width(150));
			inputs[0] = GUILayout.TextField(inputs[0]);
			inputs[0] = Regex.Replace(inputs[0], @"[^0-9 .]", "");
			if(!string.IsNullOrEmpty(inputs[0]))
				float.TryParse(inputs[0], out InitialBirthChance);
			GUILayout.EndHorizontal();
			GUILayout.Label("Birth limit");
			GUILayout.BeginHorizontal();
			GUILayout.Space(25);
			inputs[1] = GUILayout.TextField(inputs[1], GUILayout.Width(50));
			inputs[1] = Regex.Replace(inputs[1], @"[^0-9 ]", "");
			if(!string.IsNullOrEmpty(inputs[1]))
				float.TryParse(inputs[1], out BirthLimit.x);
			GUILayout.Label("to", GUILayout.Width(20));
			inputs[2] = GUILayout.TextField(inputs[2], GUILayout.Width(50));
			inputs[2] = Regex.Replace(inputs[2], @"[^0-9 ]", "");
			if(!string.IsNullOrEmpty(inputs[2]))
				float.TryParse(inputs[2], out BirthLimit.y);
			GUILayout.EndHorizontal();
			GUILayout.Label("Death limit");
			GUILayout.BeginHorizontal();
			GUILayout.Space(25);
			GUILayout.Label("-Infinity to", GUILayout.Width(60));
			inputs[3] = GUILayout.TextField(inputs[3], GUILayout.Width(50));
			inputs[3] = Regex.Replace(inputs[3], @"[^0-9 ]", "");
			if(!string.IsNullOrEmpty(inputs[3]))
				float.TryParse(inputs[3], out DeathLimit.x);
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			GUILayout.Space(25);
			inputs[4] = GUILayout.TextField(inputs[4], GUILayout.Width(50));
			inputs[4] = Regex.Replace(inputs[4], @"[^0-9 ]", "");
			if(!string.IsNullOrEmpty(inputs[4]))
				float.TryParse(inputs[4], out DeathLimit.y);
			GUILayout.Label("to Infinity", GUILayout.Width(75));
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			GUILayout.Label("Simulation steps", GUILayout.Width(125));
			inputs[5] = GUILayout.TextField(inputs[5], GUILayout.Width(50));
			inputs[5] = Regex.Replace(inputs[5], @"[^0-9 ]", "");
			if(!string.IsNullOrEmpty(inputs[5]))
				int.TryParse(inputs[5], out SimulationSteps);
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			GUILayout.Label("Grid size", GUILayout.Width(125));
			inputs[6] = GUILayout.TextField(inputs[6], GUILayout.Width(50));
			inputs[6] = Regex.Replace(inputs[6], @"[^0-9 ]", "");
			if(!string.IsNullOrEmpty(inputs[6]))
				int.TryParse(inputs[6], out GridSize);
			GUILayout.EndHorizontal();

			GUILayout.Space(20);

			if(GUILayout.Button("Initialize"))
				Initialize(true);
			if(GUILayout.Button("Next step"))
			{
				currentStep++;
				SimulateStep(true);
			}
			if(GUILayout.Button("Clean up"))
				CleanUp(true);

			GUILayout.Label("Generation time: " + generationTime.ToString("0.00") + " secs");
		}

		GUILayout.EndArea();

		if(GUI.Button(new Rect(5, Screen.height - 5 - 20, 100, 20), "Reset"))
			Reset();
	}

	#endregion



	#region PROTECTED AND PRIVATE METHODS

	/// <summary>
	/// Generates a cave with the current parameters.
	/// </summary>
	private IEnumerator Generate()
	{
		// Initialize grid
		Initialize();
		yield return new WaitForEndOfFrame();
		
		// Update cells SimulationSteps times
		for(currentStep = 0; currentStep < SimulationSteps; currentStep++)
		{
			SimulateStep();
			yield return new WaitForEndOfFrame();
		}
		
		// Clean result up
		CleanUp();
	}

	/// <summary>
	/// Initialize the grid with random values of true (rock) and false (air).
	/// </summary>
	/// <param name="_Show">If set to <c>true</c> forces the result to be instantiated.</param> 
	private void Initialize(bool _Show = false)
	{
		if(ShowDebugOutput)
			Debug.Log(this.name + ": Initializing grid.");

		// Setup grid
		grid = new bool[GridSize,GridSize];

		// Set values to true with InitialBirthChance, otherwise false
		for(int y = 0; y < grid.GetLength(1); y++)
		{
			for(int x = 0; x < grid.GetLength(0); x++)
			{
				if(Random.Range(0.0f, 1.0f) <= InitialBirthChance)
					grid[x, y] = true;
				else
					grid[x, y] = false;
			}
		}

		// Instantiate grid
		if(!ShowOnlyResult || _Show)
			InstantiateGrid(grid, "Init");
	}

	/// <summary>
	/// Executes one simulation step of the automata, updating all cells and generating a new grid.
	/// </summary>
	/// <param name="_Show">If set to <c>true</c> forces the result to be instantiated.</param> 
	private void SimulateStep(bool _Show = false)
	{
		if(ShowDebugOutput)
			Debug.Log(this.name + ": Simulating grid step. (" + (currentStep + 1) + ")");

		// Create new grid
		newGrid = new bool[grid.GetLength(0), grid.GetLength(1)];
		int neighbors = 0;

		// Iterate through current grid
		for(int x = 0; x < grid.GetLength(0); x++)
		{
			for(int y = 0; y < grid.GetLength(1); y++)
			{
				newGrid[x, y] = grid[x, y];

				// Get living neighbors
				neighbors = CountLivingNeighbors(grid, x, y);

				//Debug.Log(this.name + ": (" + x + ", " + y + ") Neighbors: " + neighbors);

				// If cell is alive, check death limits
				if(grid[x, y])
				{
					//Debug.Log(this.name + ": (" + x + ", " + y + ") Alive, checking for death limits.");

					// Check for isolation
					if(neighbors <= DeathLimit.x)
					{
						//Debug.Log(this.name + ": (" + x + ", " + y + ") Died of isolation.");
						newGrid[x, y] = false;
					}
					// Check for overpopulation
					else if(neighbors >= DeathLimit.y)
					{
						//Debug.Log(this.name + ": (" + x + ", " + y + ") Died of overpopulation.");
						newGrid[x, y] = false;
					}
				}
				// If cell is dead, check birth limit
				else if(neighbors >= BirthLimit.x && neighbors <= BirthLimit.y)
				{
					//Debug.Log(this.name + ": (" + x + ", " + y + ") Dead, being born.");
					newGrid[x, y] = true;
				}
			}
		}

		// Replace old with new grid
		grid = newGrid;

		// Instantiate new grid hiding the old
		if(!ShowOnlyResult || _Show)
			InstantiateGrid(grid, "Step " + currentStep);
	}

	/// <summary>
	/// Counts the living neighbors of the specified cell.
	/// </summary>
	/// <returns>Returns the number of living cells in a ring around the cell at position (x, y)</returns>
	/// <param name="_Grid">The grid to use</para>.</param>
	/// <param name="_X">The cell's x position.</param>
	/// <param name="_Y">The cell's y position.</param>
	private int CountLivingNeighbors(bool[,] _Grid, int _X, int _Y)
	{
		int count = 0;
		int neighborX;
		int neighborY;

		// Iterate through all neighbors
		for(int x = -1; x < 2; x++)
		{
			for(int y = -1; y < 2; y++)
			{
				// Ignore center cell
				if(x == 0 && y == 0)
					continue;

				// Get current neighbor's position
				neighborX = _X + x;
				neighborY = _Y + y;

				// Count cells off the grid as alive, as well as the outermost border
				if(neighborX < 1 || neighborX >= _Grid.GetLength(0) - 1
				        || neighborY < 1 || neighborY >= _Grid.GetLength(1) - 1)
				{
					count++;
				}
				// Check whether the current cell is alive and add it to the total
				else if(_Grid[neighborX, neighborY])
				{
					count++;
				}
			}
		}

		return count;
	}

	/// <summary>
	/// Cleans the grid up.
	/// </summary>
	/// <param name="_Show">If set to <c>true</c> forces the result to be instantiated.</param> 
	private void CleanUp(bool _Show = false)
	{
		if(ShowDebugOutput)
			Debug.Log(this.name + ": Cleaning up grid.");

		// Solidify borders
		for(int x = 0; x < grid.GetLength(0); x++)
		{
			grid[x, 0] = true;
			grid[x, grid.GetLength(1) - 1] = true;
		}
		for(int y = 0; y < grid.GetLength(1); y++)
		{
			grid[0, y] = true;
			grid[grid.GetLength(0) - 1, y] = true;
		}

		generating = false;
		generationTime = (float)DateTime.Now.Subtract(startTime).TotalMilliseconds / 1000.0f;

		// Instantiate final grid hiding the old
		if(!ShowOnlyResult || _Show)
			InstantiateGrid(grid, "CleanUp");
		else
			InstantiateGrid(grid, "Result");
	}

	/// <summary>
	/// Instantiates the grid, parenting it to an empty game object.
	/// </summary>
	/// <param name="_Grid">The grid to instantiate.</param>
	/// <param name="_Name">The name of the empty parent game object.</param>
	private void InstantiateGrid(bool[,] _Grid, string _Name)
	{
		// Hide other grids
		foreach(Transform child in transform)
			if(child.gameObject.activeInHierarchy)
				child.gameObject.SetActive(false);

		// Create new parent object
		tempGameObject = new GameObject(_Name);
		tempGameObject.transform.parent = transform;

		// Create actual grid
		InstantiateGrid(_Grid, tempGameObject.transform);
	}

	/// <summary>
	/// Instantiates the grid, parenting it to the specified transform.
	/// </summary>
	/// <param name="_Grid">The grid to instantiate.</param>
	/// <param name="_Parent">The parent transform.</param>
	private void InstantiateGrid(bool[,] _Grid, Transform _Parent)
	{
		if(ShowDebugOutput)
			Debug.Log(this.name + ": Instantiating grid.");

		int maxX = _Grid.GetLength(0);
		int maxY = _Grid.GetLength(1);

		// Iterate through the entire grid
		for(int y = 0; y < maxY; y++)
		{
			for(int x = 0; x < maxX; x++)
			{
				// Instantiate a rock if the grid value is true
				if(_Grid[x, y])
				{
					tempTransform = Instantiate(RockPrefab, new Vector3(x, y, 0.0f), Quaternion.identity) as Transform;
					tempTransform.name = "(" + x + ", " + y + ")";
					if(_Parent != null)
						tempTransform.parent = _Parent;
				}
			}
		}
	}

	/// <summary>
	/// Resets the script.
	/// </summary>
	private void Reset()
	{
		currentStep = 0;

		Camera.main.transform.position = new Vector3(GridSize / 2.0f, GridSize / 2.0f, -GridSize);
		
		StopAllCoroutines();
		foreach(Transform child in transform)
			Destroy(child.gameObject);
	}

	#endregion
}
