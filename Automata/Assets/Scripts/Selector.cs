﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class enabling the selection of a 2d or 3d automaton.
/// </summary>
public class Selector : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	public GameObject Generator2D;
	public GameObject Generator3D;

	#endregion

	#region MONOBEHAVIOUR

	void Awake()
	{
		// Deactivate 2d generator if both are active
		if(Generator3D.activeInHierarchy && Generator2D.activeInHierarchy)
			Generator2D.SetActive(false);
	}

	void LateUpdate()
	{
		// Saves a screenshot of the scene
		if(Input.GetKeyDown(KeyCode.F12))
		{
			string path = "Screenshot_" + System.DateTime.Now.ToShortDateString().Replace('/', '-') + "_" + System.DateTime.Now.ToShortTimeString().Replace(':', '-') + ".png";
			path = path.Replace(' ', '_');
			Application.CaptureScreenshot(path);
			//Debug.Log("Saved screenshot to '" + path + "'.");
		}
	}

	void OnGUI()
	{
		if(Generator3D.activeInHierarchy)
		{
			if(GUI.Button(new Rect(Screen.width - 105, Screen.height - 25, 100, 20), "Switch to 2D"))
			{
				Generator3D.SetActive(false);
				Generator2D.SetActive(true);
			}
		}
		else if(Generator2D.activeInHierarchy)
		{
			if(GUI.Button(new Rect(Screen.width - 105, Screen.height - 25, 100, 20), "Switch to 3D"))
			{
				Generator3D.SetActive(true);
				Generator2D.SetActive(false);
			}
		}
	}

	#endregion
}
